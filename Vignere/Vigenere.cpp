#include <iostream>
#include <string>
#include <algorithm>
#include <cctype>

using std::string;
using std::cin;
using std::cout;

const int SIZE = 26;
const string ENCODING = "EN";
const string DECODING = "DE";
const string EXIT = "Q";

void FillVigenereSquare(char vigenereSquare[SIZE][SIZE]) {
    for (int i = 0; i < SIZE; ++i) {
        for (int j = 0; j < SIZE; ++j) {
            vigenereSquare[i][j] = 'A' + (i + j) % SIZE;
        }
    }
}

void DisplayVigenereSquare(char vigenereSquare[SIZE][SIZE]) {
    for (int i = 0; i < SIZE; ++i) {
        for (int j = 0; j < SIZE; ++j) {
            cout << vigenereSquare[i][j] << " ";
        }
        cout << "\n";
    }
}

int GetAlphabetIndex(char c) {
    int result = c - 'A';
    return result;
}

int main()
{
    while (true) {
        cout << "Hello Vigenere!\n";
        cout << "Choose mode: en for encoding | de for decoding." << std::endl;
        cout << "OR exit with q." << std::endl;
        string mode;
        std::getline(cin, mode);
        std::transform(mode.begin(), mode.end(), mode.begin(), ::toupper);

        if (mode == EXIT) {
            cout << "Exiting." << std::endl;
            return 0;
        }

        if (mode != ENCODING && mode != DECODING) {
            cout << "Invalid mode " << mode << "." << std::endl;
            return 1;
        }

        bool encoding = true;

        if (mode == DECODING)
            encoding = false;

        char vigenere_square[SIZE][SIZE];

        FillVigenereSquare(vigenere_square);

        cout << "Vigenere Square:\n";
        DisplayVigenereSquare(vigenere_square);

        string text;
        size_t n;
        string key;
        cout << "\nText:\n";
        std::getline(cin, text);
        cout << "\nKey:\n";
        std::getline(cin, key);
        std::transform(text.begin(), text.end(), text.begin(), ::toupper);
        std::transform(key.begin(), key.end(), key.begin(), ::toupper);
        n = text.length();
        string result;

        if (encoding) {
            for (int i = 0; i < n; i++) {
                char t = text.at(i);
                char k = key.at(i % key.length());

                int text_index = GetAlphabetIndex(t);
                int key_index = GetAlphabetIndex(k);
                char r = vigenere_square[key_index][text_index];
                result += r;
            }
        }
        else {
            for (int i = 0; i < n; i++) {
                char t = text.at(i);
                char k = key.at(i % key.length());

                int key_index = GetAlphabetIndex(k);

                // Get the index of t in the vigenere row
                int text_index = -1;
                for (int j = 0; j < SIZE; j++) {
                    if (vigenere_square[key_index][j] == t) {
                        text_index = j;
                        break;
                    }
                }

                char r = 'A' + text_index;
                result += r;
            }
        }

        cout << "Result:\n" + result + "\n\n";
    }
}
